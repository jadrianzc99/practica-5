package facci.pm.zambranocenteno.practica5;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView cedula, apellido, nombre, dato;
    Button btnescribir, btnleer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cedula = (TextView) findViewById(R.id.cedula);
        apellido = (TextView)findViewById(R.id.apellido);
        nombre = (TextView)findViewById(R.id.nombre);
        dato = (TextView)findViewById(R.id.dato);
        btnescribir = (Button) findViewById(R.id.btnescribir);
        btnleer = (Button) findViewById(R.id.btnleer);

        btnescribir.setOnClickListener(this);
        btnleer.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnescribir:
                try{
                    OutputStreamWriter escritor = new OutputStreamWriter(openFileOutput("archivo.txt", Context.MODE_APPEND));
                    escritor.write(cedula.getText().toString()+","+apellido.getText().toString()+","+nombre.getText().toString());
                    escritor.close();
                }catch (Exception ex) {
                    Log.e("Archivo MI", "Error en el archivo de escritura");
                }
                break;
            case R.id.btnleer:
                try {
                    BufferedReader lector = new BufferedReader(new InputStreamReader(openFileInput("archivo.txt")));
                    String datos = lector.readLine();
                    String [] listaPersonas = datos.split(";");
                    for (int i=0; i<listaPersonas.length; i++){
                        dato.append(listaPersonas[i].split(",")[0]+" "+listaPersonas[i].split(",")[1]+" "+listaPersonas[i].split(",")[2]);
                    }

                    lector.close();
                }catch (Exception ex) {
                Log.e("Archivo MI", "Error en el archivo de escritura");
                }
                break;

        }

    }
}
